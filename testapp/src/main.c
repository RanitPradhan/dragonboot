/*****************************************************************************/

/*!
 * \file main.c
 * \brief Sample test app which blinks LED(s) of choice
 *
 */

/*****************************************************************************/

#include <stdbool.h>
#include <stdint.h>

#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_memmap.h"
#include "inc/hw_sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/can.h"
#include "inc/tm4c123gh6pm.h"

#define LED_RED GPIO_PIN_1
#define LED_BLUE GPIO_PIN_2
#define LED_GREEN GPIO_PIN_3

/* One second delay using Systick timer */
void One_Second_Delay(void)
{
    NVIC_ST_CTRL_R = 0;             /* disable SysTick during setup */
    NVIC_ST_RELOAD_R = 15999999;    /* Reload Value goes here (for 16Mhz system clock)*/
    //NVIC_ST_RELOAD_R = 9999999; /* Reload Value for 50Mhz clock, count till 10million ticks (0.2s)*/
    NVIC_ST_CTRL_R |= 0x5;          /* enable SysTick with core clock */
    while( (NVIC_ST_CTRL_R & (1<<16) ) == 0)
        ;                           /* Monitoring bit 16 to be set */
    NVIC_ST_CTRL_R = 0;             /* Disabling SysTick Timer */
}

void UART0Handler(void)
{
    int data = 0;
    data = UART0_DR_R; 
    // Wait till transmit FIFO becomes non-full
    while(UART0_FR_R & 0x0020);
    //DEBUG("Writing to TX FIFO\n");
    UART0_DR_R = data;
}

int main()
{
#if 1
    ROM_SysCtlClockSet(SYSCTL_SYSDIV_4|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);
    //ROM_SysCtlClockSet(SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    ROM_GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, LED_RED|LED_BLUE|LED_GREEN);

    for (;;) {
        // set the red LED pin high, others low
        ROM_GPIOPinWrite(GPIO_PORTF_BASE, LED_RED|LED_GREEN|LED_BLUE, LED_BLUE);
        //ROM_SysCtlDelay(5000000);
        One_Second_Delay();
        ROM_GPIOPinWrite(GPIO_PORTF_BASE, LED_RED|LED_GREEN|LED_BLUE, 0);
        //ROM_SysCtlDelay(5000000);
        One_Second_Delay();
    }
#else
    SYSCTL_RCGC2_R |= 0x00000020;
    GPIO_PORTF_DIR_R = 0x0E;
    GPIO_PORTF_DEN_R = 0x0E;
    while(1) {
        GPIO_PORTF_DATA_R = 0x0E; // For toggling all LEDs
        One_Second_Delay();
        GPIO_PORTF_DATA_R = 0;
        One_Second_Delay();
    }
#endif
}
