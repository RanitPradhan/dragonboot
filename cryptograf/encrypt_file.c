/*
                 ___====-_  _-====___
           _--^^^#####//      \\#####^^^--_
        _-^##########// (    ) \\##########^-_
       -############//  |\^^/|  \\############-
     _/############//   (@::@)   \\############\_
    /#############((     \\//     ))#############\
   -###############\\    (oo)    //###############-
  -#################\\  / VV \  //#################-
 -###################\\/      \//###################-
_#/|##########/\######(   /\   )######/\##########|\#_
|/ |#/\#/\#/\/  \#/\##\  |  |  /##/\#/  \/\#/\#/\#| \|
`  |/  V  V  `   V  \#\| |  | |/#/  V   '  V  V  \|  '
   `   `  `      `   / | |  | | \   '      '  '   '
                    (  | |  | |  )
                   __\ | |  | | /__
                  (vvv(VVV)(VVV)vvv)

                       DragonBoot

******************************************************************************/

/*!
 * \file encrypt_file.c
 * \brief Performs encryption of software binary @ host side
 */

/*****************************************************************************/

// Include files 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <string.h>
#include <openssl/rand.h>
#include "bearssl.h"
#include "inner.h"

/* Definitions of some constants */ 
#define IMAGESIZE    4
#define AES_KEY_SIZE 128  // Encrypted key size
#define SHA256_SIZE  128  // Encrypted key size
#define HEADERSIZE   (IMAGESIZE + AES_KEY_SIZE + SHA256_SIZE)

#define IMAGESIZE_OFFSET 0
#define AES_KEY_OFFSET   (IMAGESIZE_OFFSET + IMAGESIZE)
#define SHA256_OFFSET    (AES_KEY_OFFSET + AES_KEY_SIZE)
#define DATA_OFFSET      (SHA256_OFFSET + SHA256_SIZE)

/*
 * A 128-bit RSA key, generated with OpenSSL
 */
//static char *aes_key = "16403177EDCF51F9F07E2E40C15237A4";
static unsigned char aes_key[16];

/*
 * A 1024-bit RSA key, generated with OpenSSL.
 */
static const unsigned char RSA_N[] = {
	0xBF, 0xB4, 0xA6, 0x2E, 0x87, 0x3F, 0x9C, 0x8D,
	0xA0, 0xC4, 0x2E, 0x7B, 0x59, 0x36, 0x0F, 0xB0,
	0xFF, 0xE1, 0x25, 0x49, 0xE5, 0xE6, 0x36, 0xB0,
	0x48, 0xC2, 0x08, 0x6B, 0x77, 0xA7, 0xC0, 0x51,
	0x66, 0x35, 0x06, 0xA9, 0x59, 0xDF, 0x17, 0x7F,
	0x15, 0xF6, 0xB4, 0xE5, 0x44, 0xEE, 0x72, 0x3C,
	0x53, 0x11, 0x52, 0xC9, 0xC9, 0x61, 0x4F, 0x92,
	0x33, 0x64, 0x70, 0x43, 0x07, 0xF1, 0x3F, 0x7F,
	0x15, 0xAC, 0xF0, 0xC1, 0x54, 0x7D, 0x55, 0xC0,
	0x29, 0xDC, 0x9E, 0xCC, 0xE4, 0x1D, 0x11, 0x72,
	0x45, 0xF4, 0xD2, 0x70, 0xFC, 0x34, 0xB2, 0x1F,
	0xF3, 0xAD, 0x6A, 0xF0, 0xE5, 0x56, 0x11, 0xF8,
	0x0C, 0x3A, 0x8B, 0x04, 0x46, 0x7C, 0x77, 0xD9,
	0x41, 0x1F, 0x40, 0xBE, 0x93, 0x80, 0x9D, 0x23,
	0x75, 0x80, 0x12, 0x26, 0x5A, 0x72, 0x1C, 0xDD,
	0x47, 0xB3, 0x2A, 0x33, 0xD8, 0x19, 0x61, 0xE3
};
static const unsigned char RSA_E[] = {
	0x01, 0x00, 0x01
};
static const unsigned char RSA_P[] = {
	0xF2, 0xE7, 0x6F, 0x66, 0x2E, 0xC4, 0x03, 0xD4,
	0x89, 0x24, 0xCC, 0xE1, 0xCD, 0x3F, 0x01, 0x82,
	0xC1, 0xFB, 0xAF, 0x44, 0xFA, 0xCC, 0x0E, 0xAA,
	0x9D, 0x74, 0xA9, 0x65, 0xEF, 0xED, 0x4C, 0x87,
	0xF0, 0xB3, 0xC6, 0xEA, 0x61, 0x85, 0xDE, 0x4E,
	0x66, 0xB2, 0x5A, 0x9F, 0x7A, 0x41, 0xC5, 0x66,
	0x57, 0xDF, 0x88, 0xF0, 0xB5, 0xF2, 0xC7, 0x7E,
	0xE6, 0x55, 0x21, 0x96, 0x83, 0xD8, 0xAB, 0x57
};
static const unsigned char RSA_Q[] = {
	0xCA, 0x0A, 0x92, 0xBF, 0x58, 0xB0, 0x2E, 0xF6,
	0x66, 0x50, 0xB1, 0x48, 0x29, 0x42, 0x86, 0x6C,
	0x98, 0x06, 0x7E, 0xB8, 0xB5, 0x4F, 0xFB, 0xC4,
	0xF3, 0xC3, 0x36, 0x91, 0x07, 0xB6, 0xDB, 0xE9,
	0x56, 0x3C, 0x51, 0x7D, 0xB5, 0xEC, 0x0A, 0xA9,
	0x7C, 0x66, 0xF9, 0xD8, 0x25, 0xDE, 0xD2, 0x94,
	0x5A, 0x58, 0xF1, 0x93, 0xE4, 0xF0, 0x5F, 0x27,
	0xBD, 0x83, 0xC7, 0xCA, 0x48, 0x6A, 0xB2, 0x55
};
static const unsigned char RSA_DP[] = {
	0xAF, 0x97, 0xBE, 0x60, 0x0F, 0xCE, 0x83, 0x36,
	0x51, 0x2D, 0xD9, 0x2E, 0x22, 0x41, 0x39, 0xC6,
	0x5C, 0x94, 0xA4, 0xCF, 0x28, 0xBD, 0xFA, 0x9C,
	0x3B, 0xD6, 0xE9, 0xDE, 0x56, 0xE3, 0x24, 0x3F,
	0xE1, 0x31, 0x14, 0xCA, 0xBA, 0x55, 0x1B, 0xAF,
	0x71, 0x6D, 0xDD, 0x35, 0x0C, 0x1C, 0x1F, 0xA7,
	0x2C, 0x3E, 0xDB, 0xAF, 0xA6, 0xD8, 0x2A, 0x7F,
	0x01, 0xE2, 0xE8, 0xB4, 0xF5, 0xFA, 0xDB, 0x61
};
static const unsigned char RSA_DQ[] = {
	0x29, 0xC0, 0x4B, 0x98, 0xFD, 0x13, 0xD3, 0x70,
	0x99, 0xAE, 0x1D, 0x24, 0x83, 0x5A, 0x3A, 0xFB,
	0x1F, 0xE3, 0x5F, 0xB6, 0x7D, 0xC9, 0x5C, 0x86,
	0xD3, 0xB4, 0xC8, 0x86, 0xE9, 0xE8, 0x30, 0xC3,
	0xA4, 0x4D, 0x6C, 0xAD, 0xA4, 0xB5, 0x75, 0x72,
	0x96, 0xC1, 0x94, 0xE9, 0xC4, 0xD1, 0xAA, 0x04,
	0x7C, 0x33, 0x1B, 0x20, 0xEB, 0xD3, 0x7C, 0x66,
	0x72, 0xF4, 0x53, 0x8A, 0x0A, 0xB2, 0xF9, 0xCD
};
static const unsigned char RSA_IQ[] = {
	0xE8, 0xEB, 0x04, 0x79, 0xA5, 0xC1, 0x79, 0xDE,
	0xD5, 0x49, 0xA1, 0x0B, 0x48, 0xB9, 0x0E, 0x55,
	0x74, 0x2C, 0x54, 0xEE, 0xA8, 0xB0, 0x01, 0xC2,
	0xD2, 0x3C, 0x3E, 0x47, 0x3A, 0x7C, 0xC8, 0x3D,
	0x2E, 0x33, 0x54, 0x4D, 0x40, 0x29, 0x41, 0x74,
	0xBA, 0xE1, 0x93, 0x09, 0xEC, 0xE0, 0x1B, 0x4D,
	0x1F, 0x2A, 0xCA, 0x4A, 0x0B, 0x5F, 0xE6, 0xBE,
	0x59, 0x0A, 0xC4, 0xC9, 0xD9, 0x82, 0xAC, 0xE1
};

static const br_rsa_public_key RSA_PK = {
	(void *)RSA_N, sizeof RSA_N,
	(void *)RSA_E, sizeof RSA_E
};

static const br_rsa_private_key RSA_SK = {
	1024,
	(void *)RSA_P, sizeof RSA_P,
	(void *)RSA_Q, sizeof RSA_Q,
	(void *)RSA_DP, sizeof RSA_DP,
	(void *)RSA_DQ, sizeof RSA_DQ,
	(void *)RSA_IQ, sizeof RSA_IQ
};

//*****************************************************************************
/*!
 * \brief Generates a random number for the AES key
 */

int generate_random_number(void *key, int keysize)
{
    if (!RAND_bytes(key, keysize)) {
    /* OpenSSL reports a failure, act accordingly */
        printf("Failed to generate a key of size %d\n", keysize);
    }
}

//*****************************************************************************
/*!
 * \brief Checks if 2 buffers are equal in content / not
 */

static void
check_equals(const char *banner, const void *v1, const void *v2, size_t len)
{
	size_t u;
	const unsigned char *b;

	if (memcmp(v1, v2, len) == 0) {
		return;
	}
	printf("\n%s failed\n", banner);
	printf( "v1: ");
	for (u = 0, b = v1; u < len; u ++) {
		printf( "%02X", b[u]);
	}
	printf( "\nv2: ");
	for (u = 0, b = v2; u < len; u ++) {
		printf( "%02X", b[u]);
	}
	printf( "\n");
    return;
	//exit(EXIT_FAILURE);
}

//*****************************************************************************
/*!
 * \brief Performs AES encryption of a binary
 */

static void
do_AES_encrypt(char *enc_data, char *plain_data, int len,
	const br_block_cbcenc_class *ve,
	const br_block_cbcdec_class *vd)
{
    size_t u;
    int i = 0;
    unsigned char key[32];
    unsigned char iv[16];
    size_t key_len;
    br_aes_gen_cbcenc_keys v_ec;
    br_aes_gen_cbcdec_keys v_dc;
    const br_block_cbcenc_class **ec;
    const br_block_cbcdec_class **dc;

    if (ve->block_size != 16 || vd->block_size != 16
            || ve->log_block_size != 4 || vd->log_block_size != 4)
    {
        printf("AES failed: wrong block size\r");
        return;
    }

    ec = &v_ec.vtable;
    dc = &v_dc.vtable;
    key_len = sizeof(aes_key);
    memcpy(key, aes_key, sizeof(aes_key));
    ve->init(ec, key, key_len);
    memset(iv, 0, sizeof iv);
    ve->run(ec, iv, enc_data, len);
    //printf("enc_data[0] after encryption is %02x\n", enc_data[8]);
#if 0
    vd->init(dc, key, key_len);
    memset(iv, 0, sizeof iv);
    vd->run(dc, iv, enc_data, len);
    printf("enc_data[0] after decryption is %02x\n", enc_data[8]);
    printf("plain_data[0] is %02x\n", plain_data[8]);
    check_equals("KAT AES decrypt", enc_data, plain_data, len);
#endif
    printf("done.\n");
}

//*****************************************************************************
/*!
 * \brief Computes SHA256 hash value for the given data 
 */

static void compute_sha256(const void *data, int len, void *out)
{
	br_sha256_context csha256;

    br_sha256_init(&csha256);
	br_sha256_update(&csha256, data, len);
	br_sha256_out(&csha256, out);

    return;
}

//*****************************************************************************
/*!
 * \brief Creates an encrypted version of the software binary
 */

int create_encrypted_file(unsigned char *data, uint32_t size)
{
    unsigned char *enc_data = NULL;
    int *writer = NULL;
    static unsigned char hash[SHA256_SIZE];
    FILE *fd = NULL;
    int i = 0;
    int ret = 0;

    if (NULL == data || size <= 0)
    {
        printf("Sorry! Data to be encrypted doesn't exist or size was invalid !\n");
        return -1;
    }

    printf("HEADERSIZE is %d, image size is %d, totalsize is %d\n", HEADERSIZE, size, size+HEADERSIZE);
    enc_data = malloc(size+HEADERSIZE);
    memset(enc_data, 0x0, size+HEADERSIZE);

    printf("data is %p, enc_data is %p, data size is %d\n", data, enc_data, size);

    if (NULL == enc_data)
    {
        printf("Sorry! Couldn't allocate space from heap for size %d bytes\n", size+HEADERSIZE);
        return -1;
    }

    /* Set the image size as 1st 4 bytes */
    memcpy(&enc_data[IMAGESIZE_OFFSET], &size, IMAGESIZE);

    /* Generate a key for carrying out AES encryption */
    generate_random_number(aes_key, sizeof(aes_key));
    for (i = 0; i<sizeof(aes_key); i++)
        printf("AES key[%d] is %02x\n", i, aes_key[i]);

    /* Copy the AES key to the image & encrypt it using the RSA public key */
    memcpy(&enc_data[AES_KEY_OFFSET], &aes_key, sizeof(aes_key));

	if (!br_rsa_i15_public((unsigned char *)&enc_data[AES_KEY_OFFSET], AES_KEY_SIZE, &RSA_PK)) {
		printf( "RSA encryption of AES key returned 0\n");
    }

#if 0
    /* Decrypt the encrypted data's content */
	if (!br_rsa_i15_private((unsigned char *)&enc_data[AES_KEY_OFFSET], &RSA_SK)) {
		printf( "RSA decryption of AES key failed (1)\n");
        return 1;
	}

    if (memcmp(&enc_data[AES_KEY_OFFSET], aes_key, sizeof(aes_key)) == 0)
    {
        printf("AES key matches with decrypted value\n");
    }
    else
    {
        printf("AES ket doesn't match with decrypted value\n");
    }
#endif

    printf("============+> DEBUG (%s,%d) Performing mempy of app to encrypted binary \n",__FUNCTION__, __LINE__);  
    /* Encrypt the data's content */
    memcpy(&enc_data[DATA_OFFSET], data, size);

    printf("============+> DEBUG (%s,%d) Computing SHA256 hash for app \n",__FUNCTION__, __LINE__);  
    /* Compute the SHA256 hash value & store it before encrypting the data */ 
    compute_sha256(&enc_data[DATA_OFFSET], size, hash);

#if 0
    printf("============+> DEBUG (%s,%d) Performing mempy of SHA256 to encrypted binary \n",__FUNCTION__, __LINE__);  
    memcpy(&enc_data[SHA256_OFFSET], hash, sizeof(hash));
#endif

    printf("============+> DEBUG (%s,%d) Performing AES encryption of data, data offset is %d \n",__FUNCTION__, __LINE__, DATA_OFFSET);  
    /* Perform AES encryption of the data */
	do_AES_encrypt((char *)&enc_data[DATA_OFFSET], (char *)data, size,
		&br_aes_big_cbcenc_vtable,
		&br_aes_big_cbcdec_vtable);

    printf("============+> DEBUG (%s,%d) Displaying the SHA256 hash \n",__FUNCTION__, __LINE__);  
    /* Display the SHA256 hash */
    for (i = 0; i<32; i++)
        printf("Hash[%d] is %02x\n", i, hash[i]);

#if 0
    /* Encrypt the SHA256 sum with the RSA key */
	if (!br_rsa_i15_public((unsigned char *)&enc_data[SHA256_OFFSET], SHA256_SIZE, &RSA_PK)) {
		printf( "RSA encryption of the SHA256 hash failed\n");
    }
#else    
    // Sign the SHA256 hash value
    printf("============+> DEBUG (%s,%d) Signing the SHA256 hash \n",__FUNCTION__, __LINE__);  
	if (!br_rsa_i15_pkcs1_sign(BR_HASH_OID_SHA256, (const unsigned char *)hash, 32, &RSA_SK, &enc_data[SHA256_OFFSET])) {
		printf("Signature generation failed (2)\r");
        return -1;
	}
#endif

#if 0
    /* Decrypt the encrypted data's content */
	if (!br_rsa_i15_private((unsigned char *)&enc_data[SHA256_OFFSET], &RSA_SK)) {
		printf( "RSA decryption of SHA256 hash failed (1)\n");
        return 1;
	}

    if (memcmp(&enc_data[SHA256_OFFSET], hash, sizeof(hash)) == 0)
    {
        printf("SHA256 hash matches with decrypted value\n");
    }
    else
    {
        printf("SHA256 hash doesn't match with decrypted value\n");
    }
#endif

    fd = fopen("image.bin", "w+");

    if (NULL == fd)
    {
        printf("Failed to open the file image.bin\n");
        return -1;
    }

    if (fwrite(enc_data, 1, size+HEADERSIZE, fd) < (size+HEADERSIZE))
    {
        printf("Failed to write the file image.bin\n");
        return -1;
    }

    fclose(fd);
    free(enc_data);
    return 0;
}

//*****************************************************************************
/*!
 * \brief Main (entry-point)
 */

int main(int argc, char *argv[])
{
	FILE *f = NULL;
	unsigned char *data = NULL;
    long fsize = 0, nsize = 0;

    if (argc != 2)
    {
        printf("Invalid number of arguments!\n\tUsage : appgen <filename>.bin");
        return -1;
    }

    f = fopen(argv[1], "rb");
    if (NULL == f)
    {
        printf("Failed to open the file %s\n", argv[1]);
        return -1;
    }

	if (fseek(f, 0, SEEK_END) < 0)
	{
        printf("Failed to seek till end of file %s\n", argv[1]);
        return -1;
	}

	fsize = ftell(f);

#if 1
    if (fsize % 512 != 0)
    {
        nsize = (fsize/512)*512 + 512;
    }
#endif

	if (fsize <= 0)
	{
        printf("Invalid file size %ld\n", fsize);
        return -1;
	}

	if (fseek(f, 0, SEEK_SET) < 0)  /* same as rewind(f); */
	{
        printf("Failed to seek to start of file %s\n", argv[1]);
        return -1;
	}

	data = malloc(nsize);

	if (NULL == data)
	{
        printf("Failed to allocate memory of size %ld\n", fsize);
        return -1;
	}

    memset(data, 0x0, nsize);

	if (fread(data, 1, fsize, f) < fsize)
	{
        printf("Failed to read entire file %s\n", argv[1]);
        return -1;
	}

    printf("Data ptr is %p\n", data);

	if (create_encrypted_file(data, nsize) < 0)
	{
        printf("Failed to encrypt the file %s\n", argv[1]);
		free(data);
        return -1;
	}

	fclose(f);
	free(data);
	return 0;
}

//*****************************************************************************
