Steps to compile the BearSSL library sources
############################################

To compile the modified BearSSL library for ARM Cortex M3/M4 :

Make sure that the toolchain path has been appended to PATH environment
variable prior to executing the below commands :

# tar -xvf BearSSL_modified.tar.bz2

# cd BearSSL/

# make CONF=samd20

The statically linked library libbearssl.a would be created inside the folder
"samd20/".

Misc. info :
############

Base commit ID over which changes were carried out:
9721b3e7566693128a5923cc1f481216b7853466

