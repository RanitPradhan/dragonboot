/*
                 ___====-_  _-====___
           _--^^^#####//      \\#####^^^--_
        _-^##########// (    ) \\##########^-_
       -############//  |\^^/|  \\############-
     _/############//   (@::@)   \\############\_
    /#############((     \\//     ))#############\
   -###############\\    (oo)    //###############-
  -#################\\  / VV \  //#################-
 -###################\\/      \//###################-
_#/|##########/\######(   /\   )######/\##########|\#_
|/ |#/\#/\#/\/  \#/\##\  |  |  /##/\#/  \/\#/\#/\#| \|
`  |/  V  V  `   V  \#\| |  | |/#/  V   '  V  V  \|  '
   `   `  `      `   / | |  | | \   '      '  '   '
                    (  | |  | |  )
                   __\ | |  | | /__
                  (vvv(VVV)(VVV)vvv)

                       DragonBoot

******************************************************************************/

/*!
 * \file console.c
 * \brief DragonBoot interactive console 
 *
 * This file houses the implementation of the interactive console consisting of
 * shell commands & the respective functions to be invoked for the same.
 */

/*****************************************************************************/

// Include files
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h> 
#include <string.h>
#include <stdlib.h>
#include "../microrl/config.h"
#include "inc/tm4c123gh6pm.h"
#include "misc.h"
#include "platform/platform.h"
#include "swupdate/swupdate.h"
#include "printf/printf.h"

#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#define TARGET_IS_TM4C123_RB2
#include "driverlib/rom.h"

// Enable TIVA mode
#define TIVA

// Available  commands
char * keyworld [] = {_CMD_HELP, _CMD_CLEAR, _CMD_LED, _CMD_READ, _CMD_WRITE, _CMD_MMAP, _CMD_FLASH, _CMD_ERASE, _CMD_BOOT, _CMD_EXIT};
// Read/Write  subcommands
char * rw_keyworld [] = {_SCMD_MEMADDR};
// LED subcommands
char * led_keyworld [] = {_SCMD_ON, _SCMD_OFF, _SCMD_BLINK};
// LED colors
char * color_keyworld [] = {_SCMD_COLOR_RED, _SCMD_COLOR_BLUE, _SCMD_COLOR_GREEN, _SCMD_COLOR_WHITE};

#define _NUM_OF_CMD      sizeof(keyworld)/sizeof(char *)
#define _NUM_OF_LED_SCMD sizeof(led_keyworld)/sizeof(char *)
#define _NUM_OF_RW_SCMD  sizeof(rw_keyworld)/sizeof(char *)
#define _NUM_OF_BF_SCMD  sizeof(bf_keyworld)/sizeof(char *)
#define _NUM_OF_LED_COLORS sizeof(color_keyworld)/sizeof(char *)

// Array for completion
char * compl_world [_NUM_OF_CMD + 1];

// 'name' var for store some string
#define _NAME_LEN 8
char name [_NAME_LEN];
int val;

// Memory map definition for the platform
struct memory_map_struct mmap[] = 
{
    {"flash0.boot", 0x00000000, 0x00016000-1},
    {"flash0.app0", 0x00016000, 0x0001a000-1},
    {"flash0.app1", 0x0001a000, 0x0001e000-1},
    {"flash0.app2", 0x0001e000, 0x00022000-1},
    {"flash0.app3", 0x00022000, 0x00026000-1},
    {"flash0.app4", 0x00026000, 0x0002a000-1},
    {"flash0.app5", 0x0002a000, 0x0002e000-1},
    {"flash0.app6", 0x0002e000, 0x00032000-1},
    {"flash0.app7", 0x00032000, 0x00036000-1},
    {"flash0.app8", 0x00036000, 0x0003a000-1},
    {"flash0.all", 0x00016000, 0x0003a000-1},
    {"ram0.resv", 0x20000000, 0x20004000-1},
    {"ram0.app", 0x20004000, 0x20008000-1}
};

// Macro definitions for LEDs
#define LED_ADDR_BLUE     0x40025010   // Address of the blue LED
#define LED_ADDR_GREEN    0x40025020   // Address of the green LED
#define LED_ADDR_RED      0x40025008   // Address of the red LED
#define LED_ADDR_WHITE    0x40025038   // Address of all the LEDs
#define LED_ADDR_INVALID  0x40025000   // No LEDs enabled for writing

//*****************************************************************************
/*!
 * \brief Help menu
 * 
 * Prints a help menu detailing the list of commands supported & their syntaxes
 */

void print_help ()
{
    dbprintf ("Use TAB key for completion\rCommand:\r");
    dbprintf ("\tled {on | off | blink [blue/red/green/white]} - \r\t\tTurn on / turn off / blink an LED\r\r");
    dbprintf ("\tread { mem_address [hex address] [number of bytes to read] } - \r\t\tread and display <number_of_bytes> bytes from address \r\r");
    dbprintf ("\twrite { mem_address [hex address] [number of bytes to write] [content in 32-bit space separated hex words] } \r\t\t- write hex words to the specified memory address\r\r");
    dbprintf ("\tmmap - display the memory map of the device\r\r");
    dbprintf ("\tflash {flash partition name} - \r\t\tFlash new application software onto the selected flash partition\r\r");
    dbprintf ("\terase {flash partition name} - \r\t\tErase the selected flash partition\r\r");
    dbprintf ("\tboot {flash partition name} - \r\t\tBoot application software from selected flash partition\r\r");
    dbprintf ("\thelp  - this message\r\r");
    dbprintf ("\tclear - clear screen\r\r");
    dbprintf ("\texit - exit DragonBoot\r\r");
}

//*****************************************************************************
/*!
 * \brief Memory dump function (in hex)
 *
 * Print the contents of memory address from <start> address upto 
 * <num_of_bytes> specified
 */

#ifdef TIVA 
void print_memory_range(void *start, int num_of_bytes)
{
#define STRBUFSIZE 128
    size_t i;
    int counter = 0, strcnt = 0;
    static char strbuf[STRBUFSIZE];
    volatile unsigned char *masterblock = (unsigned char *) start;
    memset(strbuf, 0x0, STRBUFSIZE);
    for (i = 0; i < num_of_bytes; ++i) {
        if (i%8 == 0)
            strcnt += sprintf(strbuf, "\t%08x : ", (unsigned int)masterblock+counter);
        strcnt += sprintf(strbuf+strcnt, "%02x ", masterblock[i]);
        counter += 1;
        if ((i+1)%8 == 0) { 
            strcnt += sprintf(strbuf+strcnt, " | %04x \r", counter);
            strbuf[strcnt+1] = '\0';
            print(strbuf);
            memset(strbuf, 0x0, STRBUFSIZE);
            strcnt = 0;
        }
    }
    if ((num_of_bytes%8) != 0)
    {
        strbuf[strcnt+1] = '\0';
        print(strbuf);
        print("\r");
    }
}
#else
void print_memory_range(void *start, int num_of_bytes)
{
    size_t i;
    int counter = 0;

    volatile unsigned char *masterblock = (unsigned char *) start;
    printf("\t%p : ", (unsigned char *)masterblock + counter);
    for (i = 0; i < num_of_bytes; ++i) {
        printf("%02x ", masterblock[i]);
        if ((i+1) % 8 == 0) {
            counter+=8;
            printf(" | %04x \n\t%p : ", counter, (unsigned char *)masterblock + counter);
        }
    }
    printf("\n");
}
#endif

//*****************************************************************************
/*!
 * \brief Number format parser (string to unsigned integer)
 * 
 * Parse a number from a string, in decimal / hexadecimal format
 */

int parse_number(const char *arg, unsigned int *number)
{
    int ret = SUCCESS;
    ret = sscanf(arg, "0x%x", number);
    if (ret == 0)
    {
        DEBUG("Failed to scan as a hexadecimal number, will try decimal\r");
        ret = sscanf(arg, "%u", number);
        if (ret)
        {
            DEBUG("Decimal version is %u\r", *number);
        }
        else
        {
            ERROR("Failed to parse a number!\r");
            return ERR_BAD_NUMBER;
        }
    }
    else
    {
        DEBUG("Hex version is %x\r", *number);
    }
    return SUCCESS;
}

//*****************************************************************************
/*!
 * \brief Platform memory map dump
 *
 * Prints the memory map of the device
 */

int print_memory_map ()
{
    int i = 0;
    dbprintf ("\rMemory map for the device is as below :\r\r");
    dbprintf("Partition       Start Addr      End Addr      Size (in bytes)\r");
    dbprintf("#########       ##########      ########      #####\r\r");
    for (i = 0; i<sizeof(mmap)/sizeof(struct memory_map_struct); i++)
    {
        dbprintf("%11s     0x%08x     0x%08x     %d \r", mmap[i].name, \
                mmap[i].start_address, \
                mmap[i].end_address, \
                mmap[i].end_address - mmap[i].start_address+1);
    }
    dbprintf("\r");
    return 0;
}

//*****************************************************************************
/*!
 * \brief Memory flash partition validator
 *
 * Checks if a partition exists in the memory map table & returns its index
 * location if present.
 */

int check_partition_name(const char *name)
{
    int found = 0;
    int i = 0;

    DEBUG("name of partition to search for : %s\r", name);

    for (i = VALID_FLASHAPP_START; i<=VALID_FLASHAPP_END; i++)
    {
        // Check if the partition name passed is present in the memory map
        if (strcmp(name, mmap[i].name) == 0)
        {
            found = 1;
            break;
        }
    }
    if (!found)
    {
        dbprintf("Sorry, invalid partition chosen! See help\n\r");
        return -1;
    }

    return i;
}

//*****************************************************************************
/*!
 * \brief Microrl command execute callback function
 *
 * Execute callback for microrl library; gets invoked when a user has 
 * typed a command to be executed and presses the 'return' key.
 */

int execute (int argc, const char * const * argv)
{
    int i = 0;
    int ret = SUCCESS;
    static unsigned int addr = 0;
    static unsigned int numbytes = 0;
    static unsigned int wrdata = 0;
    int led_mode = LED_CMD_INVALID;
    int led_color = LED_COLOR_INVALID;
    uint32_t start_address = 0;
    
    // just iterate through argv word and compare it with your commands
    if (strcmp (argv[0], _CMD_HELP) == 0) {
        dbprintf("DragonBoot\r\r");
        print_help ();        // print help
    }
    else if (strcmp (argv[0], _CMD_EXIT) == 0) {
        dbprintf("Thanks for using DragonBoot, will say goodbye & come back again !\r");
        return 0;
    }
    else if (strcmp (argv[0], _CMD_CLEAR) == 0) {
        print ("\033[2J");    // ESC seq for clear entire screen
        print ("\033[H");     // ESC seq for move cursor at left-top corner
    }
    else if (strcmp (argv[0], _CMD_MMAP) == 0) {
        print_memory_map();
    }
    else if (strcmp (argv[0], _CMD_FLASH) == 0) {
        if (argc != 2)
        {
            dbprintf("Sorry, invalid number of arguments! See help\r\r");
            return 0;
        }

        i = check_partition_name(argv[1]);
        if (i<0)
            return 0;

        start_address = mmap[i].start_address;
        dbprintf ("Alright! Kindly use an XMODEM file transfer utility on your terminal to send a new application image.\r");
        dbprintf ("Please ensure that the packet size is set to 128 bytes.\r");
        dbprintf ("Shall wait till file transfer is initiated...\r");
        initialize_uart(1);
        Updater(start_address);
        disable_uart_interrupt_handler();
        return 0;
    }
    else if (strcmp (argv[0], _CMD_BOOT) == 0) {
        if (argc != 2)
        {
            dbprintf ("Sorry, invalid number of arguments! See help\n\r");
            return 0;
        }

        i = check_partition_name(argv[1]);
        if (i<0)
            return 0;

        start_address = mmap[i].start_address;

        dbprintf ("Shall boot app from flash...\r");
        BootFromFlash(start_address);
    }
    else if (strcmp (argv[0], _CMD_ERASE) == 0) {
        if (argc != 2)
        {
            dbprintf ("Sorry, invalid number of arguments! See help\n\r");
            return 0;
        }

        i = check_partition_name(argv[1]);
        if (i<0)
            return 0;

        start_address = mmap[i].start_address;

        dbprintf ("Shall erase the flash partition\r");
        erase_flash(start_address, (mmap[i].end_address - mmap[i].start_address + 1)/FLASH_SECTOR_SIZE);
    }

    else if (strcmp (argv[0], _CMD_LED) == 0) {
        if (argc != 3)
        {
            dbprintf ("Sorry, invalid number of arguments! See help\n\r");
            return 0;
        }
        for (i = 1; i < argc; i++)
        {
            switch (i)
            {
                // 2nd argument should be ON/OFF/BLINK
                case 1: 
                    if (strcmp (argv[i], _SCMD_ON) == 0)
                    {
                        led_mode = LED_ON;
                    }
                    else if (strcmp (argv[i], _SCMD_OFF) == 0)
                    {
                        led_mode = LED_OFF;
                    }
                    else if (strcmp (argv[i], _SCMD_BLINK) == 0)
                    {
                        led_mode = LED_BLINK;
                    }
                    else
                    {
                        print("Sorry, invalid led sub-command; see help.\n\r");
                    }
                    break;

                // 3rd argument should be the color of the LED 
                case 2:
                    if (strcmp (argv[i], _SCMD_COLOR_BLUE) == 0)
                    {
                        led_color = LED_COLOR_BLUE;
                    }
                    else if (strcmp (argv[i], _SCMD_COLOR_RED) == 0)
                    {
                        led_color = LED_COLOR_RED;
                    }
                    else if (strcmp (argv[i], _SCMD_COLOR_GREEN) == 0)
                    {
                        led_color = LED_COLOR_GREEN;
                    }
                    else if (strcmp (argv[i], _SCMD_COLOR_WHITE) == 0)
                    {
                        led_color = LED_COLOR_WHITE;
                    }
                    else
                    {
                        dbprintf("Sorry, invalid led color; valid colors are : blue/red/green/white.\n\r");
                    }
                    break;

                default:
                    ERROR("Illegal argument index %d!\r", i);
                    break;
            }
        }
        //DEBUG("led_mode is %d\r", led_mode);
        //DEBUG("led_color is %d\r", led_color);
        if (led_mode != LED_CMD_INVALID && led_color != LED_COLOR_INVALID)
            exec_led_command(led_mode, led_color);
    }
    else if (strcmp (argv[0], _CMD_READ) == 0) {
        if (argc < 2)
        {
            dbprintf ("Sorry, invalid number of arguments! See help\n\r");
            return 0;
        }
        if (strcmp (argv[1], _SCMD_MEMADDR) == 0){
            if (argc != 4)
            {
                dbprintf ("Sorry, invalid number of arguments! See help for usage\n\r");
                return ERR_BAD_ARGS;
            }
            for (i = 2; i<4; i++)
            {
                switch (i)
                {
                    // Get the hexadecimal address of the memory location
                    case 2:
                        //sscanf(argv[i], "0x%x", &addr);
                        DEBUG("addr is %s\r", argv[i]);
                        ret = parse_number(argv[i], &addr);
                        if (ret != SUCCESS)
                        {
                            dbprintf("Invalid address entered!\r");
                            return ret;
                        }
                        DEBUG("Memory address is 0x%x\r", addr);
                        break;

                    case 3:
                        //sscanf(argv[i], "%u", &numbytes);
                        DEBUG("numbytes is %s\r", argv[i]);
                        ret = parse_number(argv[i], &numbytes);
                        if (ret != SUCCESS)
                        {
                            dbprintf("Invalid number of bytes entered!\r");
                            return ret;
                        }
                        DEBUG("Number of bytes to be read is %u\r", numbytes);
                        break;

                    default:
                        ERROR("Illegal argument index %d!\r", i);
                        return ERR_BAD_ARGS;
                        break;
                }
            }
            print_memory_range((void *)addr, numbytes); 
        }
    }
    else if (strcmp (argv[0], _CMD_WRITE) == 0) {
        if (argc < 2)
        {
            dbprintf ("Sorry, not enough arguments! See help\n\r");
            return 0;
        }
        if (strcmp (argv[1], _SCMD_MEMADDR) == 0){
            if (argc <= 3)
            {
                dbprintf ("Sorry, not enough arguments! See help for usage\n\r");
                return ERR_BAD_ARGS;
            }
            DEBUG("addr is %s\r", argv[2]);
            ret = parse_number(argv[2], &addr);
            if (ret != SUCCESS)
            {
                dbprintf("Invalid address entered!\r");
                return ret;
            }
            DEBUG("Memory address is 0x%x\r", addr);

            for (i = 3; i<argc; i++)
            {
                //sscanf(argv[i], "%u", &numbytes);
                DEBUG("wrdata is %s\r", argv[i]);
                ret = parse_number(argv[i], (unsigned int *)&wrdata);
                if (ret != SUCCESS)
                {
                    print("Invalid data entered!\r");
                    return ret;
                }
                write_to_memory((void *)(addr+(i-3)*4), wrdata);
            }
        }
    }
    else {
        dbprintf ("command: '");
        print (argv[i]);
        dbprintf ("' Not found.\r\r");
    }

    return 0;
}

#ifdef _USE_COMPLETE

//*****************************************************************************
/*!
 * \brief Microrl command complete callback function
 *
 * Command complete callback for microrl library; gets invoked when a user has 
 * typed a command partially and presses "TAB" to auto-complete the command.
 */

char ** complet (int argc, const char * const * argv)
{
    int j = 0;

    compl_world [0] = NULL;

    // Handle all basic command names
    if (argc == 1) {
        // get last entered token
        char * bit = (char*)argv [argc-1];
        // iterate through our available token and match it
        for (int i = 0; i < _NUM_OF_CMD; i++) {
            // if token is matched (text is part of our token starting from 0 char)
            if (strstr(keyworld [i], bit) == keyworld [i]) {
                // add it to completion set
                compl_world [j++] = keyworld [i];
            }
        }
    }
    // Handle all LED specific sub-commands / options
    // LED mode
    else if (strcmp (argv[0], _CMD_LED)==0)
    {
        switch (argc)
        {
            case 2:
                // iterate through subcommand for LED command types (on/off/blink)
                for (int i = 0; i < _NUM_OF_LED_SCMD; i++) {
                    if (strstr (led_keyworld [i], argv [argc-1]) == led_keyworld [i]) {
                        compl_world [j++] = led_keyworld [i];
                    }
                }
                break;

            case 3:
                // iterate through subcommand for LED colors
                for (int i = 0; i < _NUM_OF_LED_COLORS; i++) {
                    if (strstr (color_keyworld [i], argv [argc-1]) == color_keyworld [i]) {
                        compl_world [j++] = color_keyworld [i];
                    }
                }
                break;

            default:
                break;
        }
    }
    // Handle all flash / boot / erase commands
    else if ((strcmp (argv[0], _CMD_BOOT) == 0) || (strcmp(argv[0], _CMD_FLASH) == 0)\
             || (strcmp (argv[0], _CMD_ERASE) == 0))
    {
        switch (argc)
        {
            case 2:
                // iterate through subcommand for READ command types (mem_address/string_symbol_name)
                for (int i = VALID_FLASHAPP_START; i <= VALID_FLASHAPP_END; i++) {
                    if (strstr (mmap[i].name, argv [argc-1]) == mmap[i].name) {
                        compl_world [j++] = mmap[i].name;
                    }
                }
                break;

            default:
                break;
        }
    }
    // Handle all Memory specific sub-commands / options
    else if ((strcmp (argv[0], _CMD_READ) == 0) || (strcmp(argv[0], _CMD_WRITE) == 0))
    {
        switch (argc)
        {
            case 2:
                // iterate through subcommand for READ command types (mem_address/string_symbol_name)
                for (int i = 0; i < _NUM_OF_RW_SCMD; i++) {
                    if (strstr (rw_keyworld [i], argv [argc-1]) == rw_keyworld [i]) {
                        compl_world [j++] = rw_keyworld [i];
                    }
                }
                break;

            default:
                break;
        }
    }
    else { // if there is no token in cmdline, just print all available token
        for (; j < _NUM_OF_CMD; j++) {
            compl_world[j] = keyworld [j];
        }
    }
    // note! last ptr in array always must be NULL!!!
    compl_world [j] = NULL;
    // return set of variants
    return compl_world;
}
#endif

//*****************************************************************************
