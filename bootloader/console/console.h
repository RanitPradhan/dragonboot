/*
                 ___====-_  _-====___
           _--^^^#####//      \\#####^^^--_
        _-^##########// (    ) \\##########^-_
       -############//  |\^^/|  \\############-
     _/############//   (@::@)   \\############\_
    /#############((     \\//     ))#############\
   -###############\\    (oo)    //###############-
  -#################\\  / VV \  //#################-
 -###################\\/      \//###################-
_#/|##########/\######(   /\   )######/\##########|\#_
|/ |#/\#/\#/\/  \#/\##\  |  |  /##/\#/  \/\#/\#/\#| \|
`  |/  V  V  `   V  \#\| |  | |/#/  V   '  V  V  \|  '
   `   `  `      `   / | |  | | \   '      '  '   '
                    (  | |  | |  )
                   __\ | |  | | /__
                  (vvv(VVV)(VVV)vvv)

                       DragonBoot

******************************************************************************/

/*!
 * \file console.h
 * \brief DragonBoot interactive console 
 *
 * This file houses function prototypes of the console module.
 */

void print_help ();
void print_memory_range(void *start, int num_of_bytes);
int parse_number(const char *arg, unsigned int *number);
int print_memory_map ();
int execute (int argc, const char * const * argv);
char ** complet (int argc, const char * const * argv);
int check_partition_name(const char *name);

/*****************************************************************************/
