/*
                 ___====-_  _-====___
           _--^^^#####//      \\#####^^^--_
        _-^##########// (    ) \\##########^-_
       -############//  |\^^/|  \\############-
     _/############//   (@::@)   \\############\_
    /#############((     \\//     ))#############\
   -###############\\    (oo)    //###############-
  -#################\\  / VV \  //#################-
 -###################\\/      \//###################-
_#/|##########/\######(   /\   )######/\##########|\#_
|/ |#/\#/\#/\/  \#/\##\  |  |  /##/\#/  \/\#/\#/\#| \|
`  |/  V  V  `   V  \#\| |  | |/#/  V   '  V  V  \|  '
   `   `  `      `   / | |  | | \   '      '  '   '
                    (  | |  | |  )
                   __\ | |  | | /__
                  (vvv(VVV)(VVV)vvv)

                       DragonBoot

******************************************************************************/

/*!
 * \file misc.h
 * \brief Miscellaneous definitions
 *
 * This file houses macro & enum definitions used by the console.
 */


#ifndef _MICRORL_MISC_H_
#define _MICRORL_MISC_H_

/*
Platform independent interface for implementing some specific function
for AVR, linux PC or ARM
*/

enum ErrorTypes
{
    SUCCESS = 0,
    ERR_BAD_ARGS,
    ERR_BAD_NUMBER,
    ERR_INVALID_STRING
};

// LED request types
enum RequestTypes
{
    LED_OFF = 0,
    LED_ON,
    LED_BLINK,
    LED_CMD_INVALID,
    LED_COLOR_RED,
    LED_COLOR_GREEN,
    LED_COLOR_BLUE,
    LED_COLOR_WHITE,
    LED_COLOR_INVALID
};

struct memory_map_struct
{
    char *name;
    uint32_t start_address;
    uint32_t end_address;
};

// Definition commands word
#define _CMD_HELP  "help"
#define _CMD_FLASH "flash"
#define _CMD_ERASE "erase"
#define _CMD_BOOT  "boot"
#define _CMD_MMAP  "mmap"
#define _CMD_CLEAR "clear"
#define _CMD_EXIT  "exit"
#define _CMD_LED   "led"
// Sub commands for led command
    #define _SCMD_ON              "on"
    #define _SCMD_OFF             "off"
    #define _SCMD_BLINK           "blink"
    #define _SCMD_COLOR_BLUE      "blue"
    #define _SCMD_COLOR_GREEN     "green"
    #define _SCMD_COLOR_RED       "red"
    #define _SCMD_COLOR_WHITE     "white"
#define _CMD_READ  "read"
#define _CMD_WRITE "write"
// Sub commands for read/write command
    #define _SCMD_MEMADDR         "mem_address"

// Valid flash app locations (start & end)
#define VALID_FLASHAPP_START 1
#define VALID_FLASHAPP_END   10
#define FLASH_APP_SECTORS    16
#define FLASH_SECTOR_SIZE    1024

#endif
/*****************************************************************************/
