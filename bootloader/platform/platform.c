/*
                 ___====-_  _-====___
           _--^^^#####//      \\#####^^^--_
        _-^##########// (    ) \\##########^-_
       -############//  |\^^/|  \\############-
     _/############//   (@::@)   \\############\_
    /#############((     \\//     ))#############\
   -###############\\    (oo)    //###############-
  -#################\\  / VV \  //#################-
 -###################\\/      \//###################-
_#/|##########/\######(   /\   )######/\##########|\#_
|/ |#/\#/\#/\/  \#/\##\  |  |  /##/\#/  \/\#/\#/\#| \|
`  |/  V  V  `   V  \#\| |  | |/#/  V   '  V  V  \|  '
   `   `  `      `   / | |  | | \   '      '  '   '
                    (  | |  | |  )
                   __\ | |  | | /__
                  (vvv(VVV)(VVV)vvv)

                       DragonBoot

******************************************************************************/

/*!
 * \file platform.c
 * \brief All board-specific functionality defined here
 *
 * This file houses the implementation of board specific initialization
 * & peripheral exercising routines like UART, interrupt & GPIO settings etc.
 */

/*****************************************************************************/

// Include files
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h> 
#include <string.h>
#include <stdlib.h>
#include "inc/tm4c123gh6pm.h"
#include "printf/printf.h"
#include "console/misc.h"
#include "microrl/config.h"
#include "ringbuffer/ringbuffer.h"
#include "platform/platform.h"

#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#define TARGET_IS_TM4C123_RB2
#include "driverlib/rom.h"

// Macro definitions for LEDs
#define LED_ADDR_BLUE     0x40025010   // Address of the blue LED
#define LED_ADDR_GREEN    0x40025020   // Address of the green LED
#define LED_ADDR_RED      0x40025008   // Address of the red LED
#define LED_ADDR_WHITE    0x40025038   // Address of all the LEDs
#define LED_ADDR_INVALID  0x40025000   // No LEDs enabled for writing

// Ring buffer handle
static ring_buffer_t ring_buffer_handle;

// LED GPIO address for actuating blink operation 
// (via SysTick interrupt handler)
unsigned int led_blink_addr;

//*****************************************************************************
/*!
 * \brief Enables interrupts globally
 */

void EnableInterrupts(void)
{
    __asm  ("CPSIE  I\n");
}

//*****************************************************************************
/*!
 * \brief Disables interrupts globally
 */

void DisableInterrupts(void)
{
    __asm ("CPSID  I\n");
}

//*****************************************************************************
/*!
 * \brief UART0 interrupt handler
 *
 * This function is used to capture UART packets in interrupt mode, specifically
 * on reception of data (RX line), and adding the received data to a software
 * ring buffer. This mode of operation is critical for the working of data 
 * transfer during software update via XMODEM protocol.
 */

void UART0Handler(void)
{
    int data = 0;
    data = UART0_DR_R; 
    ring_buffer_queue(&ring_buffer_handle, (char)data);
    //DEBUG("Data : %c\r", data&0xff);
    UART0_ICR_R |= (0x01 << 4);     // Clear RX interrupt
}

//*****************************************************************************
/*!
 * \brief Push a byte out via TX line of UART
 *
 * This function is used to push a character out through the TX line of UART,
 * used in association with the XMODEM protocol.
 */

void _outbyte(int c)
{
    // Wait till transmit FIFO becomes non-full
    while(UART0_FR_R & 0x0020);

    //DEBUG("Writing to TX FIFO\r");
    UART0_DR_R = c;

}

//*****************************************************************************
/*!
 * \brief Receive a byte through UART
 *
 * This function is used to receive a byte stored in the ring buffer asynchronously
 * by the UART interrupt handler while data is being received through XMODEM
 * protocol.
 */

int _inbyte(unsigned short timeout) // msec timeout
{
    int data = 0;
    int ret = 0;

    // Critical section as the ring buffer is shared by app & interrupt handler
    DisableInterrupts();
    ret = ring_buffer_dequeue(&ring_buffer_handle, (char *)&data); 
    EnableInterrupts();
    if (ret == 0)
    {
        // The ring buffer was empty, so wait a while & try again
        timer0A_delayMs_periodic(timeout);
        
        // Critical section as the ring buffer is shared by app & interrupt handler
        DisableInterrupts();
        ret = ring_buffer_dequeue(&ring_buffer_handle, (char *)&data); 
        EnableInterrupts();

        if (0 == ret)
            data = -1;
    }
    return data;
}

//*****************************************************************************
/*!
 * \brief Ring buffer initialization routine
 */

void initialize_ringbuffer()
{
    DEBUG("Initialized the ring buffer\r");
    ring_buffer_init(&ring_buffer_handle);
}

//*****************************************************************************
/*!
 * \brief Disables UART0
 *
 * This function is invoked before booting an application, done as a 
 * precautionary measure.
 */

void disable_uart()
{
    // Disable UART interrupt
    NVIC_EN0_R &= ~0x00000020;   /*  Disable interrupt 5 in NVIC */

    // Disable UART0
    UART0_CTL_R = 0;

    // Disable clock to UART0
    SYSCTL_RCGCUART_R &= ~0x01;

    // Disable clock to PORTA
    SYSCTL_RCGCGPIO_R &= ~0x01;
}

//*****************************************************************************
/*!
 * \brief Disables UART0 interrupt mode
 *
 * This function is invoked post software update to resume UART command
 * reception via polling mechanism.
 */

int disable_uart_interrupt_handler()
{
    // Set UART interrupt mask to disable RX interrupt
    UART0_IM_R &= ~0x10;
    //
    // Set interrupt priority & enable the same for UART0
    NVIC_EN0_R &= ~0x00000020;        /*  Disable interrupt 5 in NVIC */

    return 0;
}

//*****************************************************************************
/*!
 * \brief Enables UART0 interrupt mode
 *
 * This function is used to enable UART interrupt handler for receiving 
 * data packets during software upgrade mode; so that data reception latency
 * is at its least and no packets are lost.
 */

int enable_uart_interrupt_handler()
{
    // Set UART interrupt mask to handle only RX
    UART0_IM_R |= 0x10;
    //
    // Set interrupt priority & enable the same for UART0
    NVIC_PRI1_R = (NVIC_PRI1_R & 0xFFFF1FFF) | 0x0000A000; /*  priority 5 */
    NVIC_EN0_R |= 0x00000020;        /*  Enable interrupt 5 in NVIC */

    EnableInterrupts();             /* Enable global Interrupt flag (I) */
    return 0;
}

//*****************************************************************************
/*!
 * \brief UART0 initialization routine
 *
 * This function is used to configure the UART0 port of the platform, set the
 * rate to 115200bps & enable it for TX/RX.
 */

void initialize_uart(int interrupt_mode)
{
    // Provide clock to UART0
    SYSCTL_RCGCUART_R |= 0x01;

    // Provide clock to PORTA
    SYSCTL_RCGCGPIO_R |= 0x01;

    // Disable UART0
    UART0_CTL_R = 0;

    // Set UART0's integer part for 115200bps
    UART0_IBRD_R = 8;

    // Set UART0's fractional part for 115200bps
    UART0_FBRD_R = 44;

    // Configure the line control value for 1 stop bit, no FIFO,
    // no interrupt, no parity, and 8-bit data size
    UART0_LCRH_R = 0x60;

    // Enable UART interrupt handler
    if (interrupt_mode)
        enable_uart_interrupt_handler();

    // Set UART0 Enable, TXen & RXen bits
    UART0_CTL_R = 0x0301;

    // Enable ports PA0 & PA1 as Digital I/O
    GPIO_PORTA_DEN_R |= 0x03;

    // Select alternate functionality for PA0 & PA1
    GPIO_PORTA_AFSEL_R |= 0x03;

    // Select UART as alternate functionality for PA0 & PA1
    GPIO_PORTA_PCTL_R = 0x11;

    // Disable analog mode for port A[1:0]
    GPIO_PORTA_AMSEL_R &= 0xfffffffC;

    DEBUG("Finished configuring UART0\r");
}

//*****************************************************************************
/*!
 * \brief Receive packets through UART0
 */

void UARTReceive(unsigned char *pui8Data, unsigned int ui32Size)
{
    //
    // Send out the number of bytes requested.
    //
    while(ui32Size--)
    {
        //
        // Wait for the FIFO to not be empty.
        //
        while(UART0_FR_R & 0x0010);

        // Read data from the UART0 data register
        *pui8Data++ = (0x00ff & UART0_DR_R);
    }
}

//*****************************************************************************
/*!
 * \brief Flush the packets sent on UART TX line
 */

void UARTFlush()
{
    //
    // Wait for the UART FIFO to empty and then wait for the shifter to get the
    // bytes out the port.
    //
    while(!(UART0_FR_R & 0x80))
    {
    }

    //
    // Wait for the FIFO to not be busy so that the shifter completes.
    //
    while(UART0_FR_R & 0x08)
    {
    }
}

//*****************************************************************************
/*!
 * \brief Send packets via UART
 */

void UARTSend(const unsigned char *pui8Data, unsigned int ui32Size)
{
    //
    // Transmit the number of bytes requested on the UART port.
    //
    while(ui32Size--)
    {
        // Wait till transmit FIFO becomes non-full
        while(UART0_FR_R & 0x0020);

        //DEBUG("Writing to TX FIFO\r");
        UART0_DR_R = *pui8Data++;
    }

    //
    // Wait until the UART is done transmitting.
    //
    UARTFlush();
}

//*****************************************************************************
/*!
 * \brief Perform hardware specific initialization routines
 *
 * This function is invoked prior to Microrl initialization, to configure
 * the hardware before entering the CLI.
 */

void init (void)
{
    initialize_uart(0);
    initialize_ringbuffer();
    initialize_LEDs();
}

//*****************************************************************************
/*!
 * \brief Print callback for the Microrl library
 *
 * This function is invoked whenever the Microrl / Console state machine
 * wishes to print some data to the console.
 */

void print (const char * str)
{
    unsigned char *ptr = (unsigned char *)str;
    unsigned char ch = '\n';
    for (ptr = (unsigned char *)str; (ptr != NULL) && (*ptr != '\0'); ptr++)
    {
        UARTSend(ptr, 1);
        if (*ptr == '\r')
            UARTSend(&ch, 1);
    }
}

/*!
 * \brief Gets a character keyed by the user
 *
 * Gets char that the user pressed, blocking call
 */

char get_char (void)
{
    int ch;
    UARTReceive((unsigned char *)&ch, 1);
    return ch;
}

//*****************************************************************************
/*!
 * \brief Delay function (in milliseconds)
 *
 * Delay in multiple of milliseconds using periodic mode with HW timer.
 */

void timer0A_delayMs_periodic(int ttime)
{
    int i;

    SYSCTL_RCGCTIMER_R |= 1;     /* enable clock to Timer Block 0 */

    TIMER0_CTL_R = 0;            /* disable Timer before initialization */
    TIMER0_CFG_R = 0x04;         /* 16-bit option */
    TIMER0_TAMR_R = 0x02;        /* periodic mode and down-counter */
    TIMER0_TAILR_R = 16000 - 1;  /* Timer A interval load value register */
    TIMER0_ICR_R = 0x1;          /* clear the TimerA timeout flag*/
    TIMER0_CTL_R |= 0x01;        /* enable Timer A after initialization */

    for(i = 0; i < ttime; i++) {
        while ((TIMER0_RIS_R & 0x1) == 0)
            ;                    /* wait for TimerA timeout flag */

        TIMER0_ICR_R = 0x1;      /* clear the TimerA timeout flag */
    }
}

//*****************************************************************************
/*!
 * \brief SigInt handler (Ctrl+C)
 */

void sigint (void)
{
    dbprintf ("^C caught!\r\r");
}

//*****************************************************************************
/*!
 * \brief LED Initialization function
 */

void initialize_LEDs ()
{
    /* Configure Port F for accessing the LEDs */
    SYSCTL_RCGC2_R |= 0x00000020;
    GPIO_PORTF_DIR_R = 0x0E;
    GPIO_PORTF_DEN_R = 0x0E;

    /* Configure the SysTick timer for blinking the LEDs @ 1Hz */
    NVIC_ST_RELOAD_R = 16000000-1;  /* reload with number of clocks per second */
    NVIC_ST_CTRL_R = 7;             /* enable SysTick interrupt, use system clock */
 
    led_blink_addr = LED_ADDR_INVALID;

    enable_irq();                   /* global enable interrupt */
}

//*****************************************************************************
/*!
 * \brief SysTick interrupt handler
 *
 * This interrupt handler is used to toggle the LED selected by the user
 * during a "blink" request
 */

void SysTick_Handler(void)
{
    *((volatile unsigned int *)led_blink_addr) ^= 0xff; /* toggle the desired LED */
}
 
//*****************************************************************************
/*!
 * \brief Enables interrupts
 */

void enable_irq(void)
{
    __asm  ("    CPSIE  I\n");
}

//*****************************************************************************
/*!
 * \brief Disables interrupts
 */

void disable_irq(void)
{
    __asm ("CPSID  I\n");
}

//*****************************************************************************
/*!
 * \brief Delay in microseconds (software delay)
 */

void delayUs(int n)
{
    int i, j;

    for( i = 0 ; i < n; i++ )
        for( j = 0; j < 3; j++ ) {} /* do nothing for 1 us */
}

//*****************************************************************************
/*!
 * \brief Delay in milliseconds (software delay)
 */

void delayMs(int n)
{
    int i, j;
    for(i = 0 ; i < n; i++)
        for(j = 0; j < 3180; j++) {}
}

//*****************************************************************************
/*!
 * \brief Translates an LED selection to its equivalent GPIO port address
 */

unsigned int led_color_map(int led_color)
{
    unsigned int addr = LED_ADDR_INVALID;
    switch (led_color)
    {
        case LED_COLOR_BLUE:
            addr = LED_ADDR_BLUE;
            break;

        case LED_COLOR_GREEN:
            addr = LED_ADDR_GREEN;
            break;

        case LED_COLOR_RED:
            addr = LED_ADDR_RED;
            break;

        case LED_COLOR_WHITE:
            addr = LED_ADDR_WHITE;
            break;

        case LED_COLOR_INVALID:
            addr = LED_ADDR_INVALID;
            break;

        default:
            break;
    }

    return addr;
}

//*****************************************************************************
/*!
 * \brief Executes an LED ON / OFF / BLINK operation
 */

void exec_led_command(int led_mode, int led_color)
{
    unsigned int addr = LED_ADDR_INVALID;

    addr = led_color_map(led_color);

    switch (led_mode)
    {
        case LED_ON:
            disable_irq();
            *((volatile unsigned int *)LED_ADDR_WHITE) = 0;
            led_blink_addr = LED_ADDR_INVALID;
            *((volatile unsigned int *)addr) = 0xff;
            break;

        case LED_OFF:
            disable_irq();
            led_blink_addr = LED_ADDR_INVALID;
            *((volatile unsigned int *)addr) = 0;
            break;

        case LED_BLINK:
            disable_irq();
            *((volatile unsigned int *)LED_ADDR_WHITE) = 0;
            led_blink_addr = addr;
            enable_irq();
            break;

        default:
            break;
    }
}

//*****************************************************************************
/*!
 * \brief Write a 4-byte word to a memory location
 */

int write_to_memory(void *address, unsigned int wrdata)
{
    volatile unsigned int *destination = (unsigned int *) address;

    DEBUG("Writing %x to address %p\r", (unsigned int)wrdata, destination);

    *destination = wrdata;

    return SUCCESS;
}
//*****************************************************************************
