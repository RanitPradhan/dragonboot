/*
                 ___====-_  _-====___
           _--^^^#####//      \\#####^^^--_
        _-^##########// (    ) \\##########^-_
       -############//  |\^^/|  \\############-
     _/############//   (@::@)   \\############\_
    /#############((     \\//     ))#############\
   -###############\\    (oo)    //###############-
  -#################\\  / VV \  //#################-
 -###################\\/      \//###################-
_#/|##########/\######(   /\   )######/\##########|\#_
|/ |#/\#/\#/\/  \#/\##\  |  |  /##/\#/  \/\#/\#/\#| \|
`  |/  V  V  `   V  \#\| |  | |/#/  V   '  V  V  \|  '
   `   `  `      `   / | |  | | \   '      '  '   '
                    (  | |  | |  )
                   __\ | |  | | /__
                  (vvv(VVV)(VVV)vvv)

                       DragonBoot

******************************************************************************/

/*!
 * \file platform.h
 * \brief Function prototypes specific to hardware routines
 *
 * This file houses platform specific function routine prototypes.
 */

/*****************************************************************************/

void EnableInterrupts(void);
void DisableInterrupts(void);
void UART0Handler(void);
void _outbyte(int c);
int _inbyte(unsigned short timeout);
void initialize_ringbuffer();
void disable_uart();
int disable_uart_interrupt_handler();
int enable_uart_interrupt_handler();
void initialize_uart(int interrupt_mode);
void UARTReceive(unsigned char *pui8Data, unsigned int ui32Size);
void UARTSend(const unsigned char *pui8Data, unsigned int ui32Size);
void UARTFlush();
void init (void);
void print (const char * str);
char get_char (void);
void timer0A_delayMs_periodic(int ttime);
void sigint (void);
void initialize_LEDs ();
void SysTick_Handler(void);
void enable_irq(void);
void disable_irq(void);
void delayUs(int n);
void delayMs(int n);
unsigned int led_color_map(int led_color);
void exec_led_command(int led_mode, int led_color);
int write_to_memory(void *address, unsigned int wrdata);

/*****************************************************************************/
