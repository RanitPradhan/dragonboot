/*
                 ___====-_  _-====___
           _--^^^#####//      \\#####^^^--_
        _-^##########// (    ) \\##########^-_
       -############//  |\^^/|  \\############-
     _/############//   (@::@)   \\############\_
    /#############((     \\//     ))#############\
   -###############\\    (oo)    //###############-
  -#################\\  / VV \  //#################-
 -###################\\/      \//###################-
_#/|##########/\######(   /\   )######/\##########|\#_
|/ |#/\#/\#/\/  \#/\##\  |  |  /##/\#/  \/\#/\#/\#| \|
`  |/  V  V  `   V  \#\| |  | |/#/  V   '  V  V  \|  '
   `   `  `      `   / | |  | | \   '      '  '   '
                    (  | |  | |  )
                   __\ | |  | | /__
                  (vvv(VVV)(VVV)vvv)

                       DragonBoot

******************************************************************************/

/*!
 * \file swupdate.c
 * \brief Software update related functions
 *
 * This file houses the implementation of software update & flash management
 * routines.
 */

/*****************************************************************************/

// Include files
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include "microrl/config.h"
#include "console/misc.h"
#include "swupdate/swupdate.h"
#include "console/console.h"
#include "xmodem/xmodem.h"
#include "platform/platform.h"
#include "inc/tm4c123gh6pm.h"
#include "bearssl.h"
#include "inner.h"
#define TARGET_IS_TM4C123_RB2
#include "driverlib/rom.h"

// Definitions of some constants 
#define APP_START_ADDR 0x20004000  // App will be copied to this RAM address
#define MAX_APP_SIZE   (16*1024)   // Max app size - 16KB
#define IMAGESIZE    4
#define AES_KEY_SIZE 128           // Encrypted key size in transmitted bin
#define SHA256_SIZE  128           // Encrypted key size in transmitted bin
#define HEADERSIZE   (IMAGESIZE + AES_KEY_SIZE + SHA256_SIZE)
#define IMAGESIZE_OFFSET 0
#define AES_KEY_OFFSET   (IMAGESIZE_OFFSET + IMAGESIZE)
#define SHA256_OFFSET    (AES_KEY_OFFSET + AES_KEY_SIZE)
#define DATA_OFFSET      (SHA256_OFFSET + SHA256_SIZE)

//*****************************************************************************
//
// Holds the RSA private keys used for decryption of new software image
//
//*****************************************************************************

/*
 * A 1024-bit RSA key, generated with OpenSSL.
 */
static const unsigned char RSA_N[] = {
	0xBF, 0xB4, 0xA6, 0x2E, 0x87, 0x3F, 0x9C, 0x8D,
	0xA0, 0xC4, 0x2E, 0x7B, 0x59, 0x36, 0x0F, 0xB0,
	0xFF, 0xE1, 0x25, 0x49, 0xE5, 0xE6, 0x36, 0xB0,
	0x48, 0xC2, 0x08, 0x6B, 0x77, 0xA7, 0xC0, 0x51,
	0x66, 0x35, 0x06, 0xA9, 0x59, 0xDF, 0x17, 0x7F,
	0x15, 0xF6, 0xB4, 0xE5, 0x44, 0xEE, 0x72, 0x3C,
	0x53, 0x11, 0x52, 0xC9, 0xC9, 0x61, 0x4F, 0x92,
	0x33, 0x64, 0x70, 0x43, 0x07, 0xF1, 0x3F, 0x7F,
	0x15, 0xAC, 0xF0, 0xC1, 0x54, 0x7D, 0x55, 0xC0,
	0x29, 0xDC, 0x9E, 0xCC, 0xE4, 0x1D, 0x11, 0x72,
	0x45, 0xF4, 0xD2, 0x70, 0xFC, 0x34, 0xB2, 0x1F,
	0xF3, 0xAD, 0x6A, 0xF0, 0xE5, 0x56, 0x11, 0xF8,
	0x0C, 0x3A, 0x8B, 0x04, 0x46, 0x7C, 0x77, 0xD9,
	0x41, 0x1F, 0x40, 0xBE, 0x93, 0x80, 0x9D, 0x23,
	0x75, 0x80, 0x12, 0x26, 0x5A, 0x72, 0x1C, 0xDD,
	0x47, 0xB3, 0x2A, 0x33, 0xD8, 0x19, 0x61, 0xE3
};
static const unsigned char RSA_E[] = {
	0x01, 0x00, 0x01
};
static const unsigned char RSA_P[] = {
	0xF2, 0xE7, 0x6F, 0x66, 0x2E, 0xC4, 0x03, 0xD4,
	0x89, 0x24, 0xCC, 0xE1, 0xCD, 0x3F, 0x01, 0x82,
	0xC1, 0xFB, 0xAF, 0x44, 0xFA, 0xCC, 0x0E, 0xAA,
	0x9D, 0x74, 0xA9, 0x65, 0xEF, 0xED, 0x4C, 0x87,
	0xF0, 0xB3, 0xC6, 0xEA, 0x61, 0x85, 0xDE, 0x4E,
	0x66, 0xB2, 0x5A, 0x9F, 0x7A, 0x41, 0xC5, 0x66,
	0x57, 0xDF, 0x88, 0xF0, 0xB5, 0xF2, 0xC7, 0x7E,
	0xE6, 0x55, 0x21, 0x96, 0x83, 0xD8, 0xAB, 0x57
};
static const unsigned char RSA_Q[] = {
	0xCA, 0x0A, 0x92, 0xBF, 0x58, 0xB0, 0x2E, 0xF6,
	0x66, 0x50, 0xB1, 0x48, 0x29, 0x42, 0x86, 0x6C,
	0x98, 0x06, 0x7E, 0xB8, 0xB5, 0x4F, 0xFB, 0xC4,
	0xF3, 0xC3, 0x36, 0x91, 0x07, 0xB6, 0xDB, 0xE9,
	0x56, 0x3C, 0x51, 0x7D, 0xB5, 0xEC, 0x0A, 0xA9,
	0x7C, 0x66, 0xF9, 0xD8, 0x25, 0xDE, 0xD2, 0x94,
	0x5A, 0x58, 0xF1, 0x93, 0xE4, 0xF0, 0x5F, 0x27,
	0xBD, 0x83, 0xC7, 0xCA, 0x48, 0x6A, 0xB2, 0x55
};
static const unsigned char RSA_DP[] = {
	0xAF, 0x97, 0xBE, 0x60, 0x0F, 0xCE, 0x83, 0x36,
	0x51, 0x2D, 0xD9, 0x2E, 0x22, 0x41, 0x39, 0xC6,
	0x5C, 0x94, 0xA4, 0xCF, 0x28, 0xBD, 0xFA, 0x9C,
	0x3B, 0xD6, 0xE9, 0xDE, 0x56, 0xE3, 0x24, 0x3F,
	0xE1, 0x31, 0x14, 0xCA, 0xBA, 0x55, 0x1B, 0xAF,
	0x71, 0x6D, 0xDD, 0x35, 0x0C, 0x1C, 0x1F, 0xA7,
	0x2C, 0x3E, 0xDB, 0xAF, 0xA6, 0xD8, 0x2A, 0x7F,
	0x01, 0xE2, 0xE8, 0xB4, 0xF5, 0xFA, 0xDB, 0x61
};
static const unsigned char RSA_DQ[] = {
	0x29, 0xC0, 0x4B, 0x98, 0xFD, 0x13, 0xD3, 0x70,
	0x99, 0xAE, 0x1D, 0x24, 0x83, 0x5A, 0x3A, 0xFB,
	0x1F, 0xE3, 0x5F, 0xB6, 0x7D, 0xC9, 0x5C, 0x86,
	0xD3, 0xB4, 0xC8, 0x86, 0xE9, 0xE8, 0x30, 0xC3,
	0xA4, 0x4D, 0x6C, 0xAD, 0xA4, 0xB5, 0x75, 0x72,
	0x96, 0xC1, 0x94, 0xE9, 0xC4, 0xD1, 0xAA, 0x04,
	0x7C, 0x33, 0x1B, 0x20, 0xEB, 0xD3, 0x7C, 0x66,
	0x72, 0xF4, 0x53, 0x8A, 0x0A, 0xB2, 0xF9, 0xCD
};
static const unsigned char RSA_IQ[] = {
	0xE8, 0xEB, 0x04, 0x79, 0xA5, 0xC1, 0x79, 0xDE,
	0xD5, 0x49, 0xA1, 0x0B, 0x48, 0xB9, 0x0E, 0x55,
	0x74, 0x2C, 0x54, 0xEE, 0xA8, 0xB0, 0x01, 0xC2,
	0xD2, 0x3C, 0x3E, 0x47, 0x3A, 0x7C, 0xC8, 0x3D,
	0x2E, 0x33, 0x54, 0x4D, 0x40, 0x29, 0x41, 0x74,
	0xBA, 0xE1, 0x93, 0x09, 0xEC, 0xE0, 0x1B, 0x4D,
	0x1F, 0x2A, 0xCA, 0x4A, 0x0B, 0x5F, 0xE6, 0xBE,
	0x59, 0x0A, 0xC4, 0xC9, 0xD9, 0x82, 0xAC, 0xE1
};
static const br_rsa_private_key RSA_SK = {
	1024,
	(void *)RSA_P, sizeof RSA_P,
	(void *)RSA_Q, sizeof RSA_Q,
	(void *)RSA_DP, sizeof RSA_DP,
	(void *)RSA_DQ, sizeof RSA_DQ,
	(void *)RSA_IQ, sizeof RSA_IQ
};

static const br_rsa_public_key RSA_PK = {
	(void *)RSA_N, sizeof RSA_N,
	(void *)RSA_E, sizeof RSA_E
};

//*****************************************************************************
//
// Holds the current status of the last command that was issued to the boot
// loader.
//
//*****************************************************************************
unsigned char g_ui8Status;

//*****************************************************************************
//
// This holds the current remaining size in bytes to be downloaded.
//
//*****************************************************************************
unsigned int g_ui32TransferSize;
unsigned int g_ui32ImageSize;

//*****************************************************************************
//
// This holds the current address that is being written to during a download
// command.
//
//*****************************************************************************
unsigned int g_ui32TransferAddress;
unsigned int g_ui32ImageAddress;

//*****************************************************************************
//
// This is the packet data buffer used during transfers to the boot loader.
//
//*****************************************************************************
unsigned int g_pui32DataBuffer[20];

//*****************************************************************************
//
// This is the data buffer used to store the entire binary sent via UART.
//
//*****************************************************************************
//unsigned char g_ui8AppBuffer[2048];
unsigned char *g_ui8AppBuffer = (unsigned char *)APP_START_ADDR;

//*****************************************************************************
//
// This is an specially aligned buffer pointer to g_pui32DataBuffer to make
// copying to the buffer simpler.  It must be offset to end on an address that
// ends with 3.
//
//*****************************************************************************
unsigned char *g_pui8DataBuffer;

//*****************************************************************************
//
// Define the AES key used for decryption of app content
//
//*****************************************************************************
//static char *aes_key = "16403177EDCF51F9F07E2E40C15237A4";
static unsigned char aes_key[16];

//
//*****************************************************************************
//
//! This function boots from the default flash app location 
//!
//! This function is called after an app binary has been successfully received
//! received via UART and written to flash
//
//! \return Never returns.
//
//*****************************************************************************

void BootFromFlash(uint32_t start_address)
{
    uint32_t *app_address = (uint32_t *)APP_START_ADDR;
    uint32_t *flash_address = (uint32_t *)start_address;

    // Check if the partition has a valid application image / not
    if (*flash_address == 0xffffffff)
    {
        dbprintf("This flash partition doesn't have a valid image\r");
        dbprintf("Aborting application boot, returning to DragonBoot console...\r");
        return;
    }

    // Disable UART0 & associated interrupt 
    DisableInterrupts();
    disable_uart();

    memcpy(app_address, flash_address, 2048);

    //DEBUG("Updating the NVIC interrupt vector offset table\r");
    //DEBUG("NVIC vector table offset address is %08x\r", APP_START_ADDR);
    NVIC_VTABLE_R = APP_START_ADDR;

#if 0
    //DEBUG("app_address[1] is %p\r", &app_address[1]);
    ( ( void ( * )( void ) )app_address[1] )( ) ;
#else
    // Load the stack pointer from the application's vector table.
    //
    __asm("    ldr     r0, =0x20004000\n"
          "    ldr     r1, [r0]\n"
          "    mov     sp, r1");


    // Load the initial PC from the application's vector table and branch to
    // the application's entry point.
    //
    __asm("    ldr     r0, [r0, #4]\n"
          "    bx      r0\n");

#endif    
    while(1)
    {
    }
}

//*****************************************************************************
//
//! This function erases the flash sectors as requested
//!
//! This function is called either during updation of an application software
//  to a flash location / during manual erasure of a flash partition as
//  requested by the user.
//
//! \return Returns success / error status 
//
//*****************************************************************************

int erase_flash(uint32_t address, int num_of_sectors)
{
    uint32_t erase_address = 0;
    int status = 0;
    int i;

    status = 0;

    DEBUG("Start address : %08x\r", address);
    DEBUG("Number of sectors to erase : %d\r", num_of_sectors);
    for (i = 0; i<num_of_sectors; i++)
    {
        erase_address = (address + i*0x400);
        DEBUG("Erasing 1k sector #%d (Address %08x)\r", i, erase_address);
        status = ROM_FlashErase(erase_address);

        if (status != 0)
        {
            ERROR("Failed to erase sector #%d (address %08x)\r", i, erase_address);
            return -1;
        }
    }

    return 0;
}

//*****************************************************************************
//
//! This function updates the flash app location with new app's binary
//!
//! This function is called after an app binary has been successfully received
//! received via UART as part of the software update process.
//
//! \return Returns nothing
//
//*****************************************************************************

void update_flash(uint32_t address, void *data, int datasize)
{
    int status = 0;
#ifdef _DEBUG
    volatile uint32_t *verify = (uint32_t *)address;
#endif

    DEBUG("Flash data has not been updated, shall update the same\r");

    // Each flash partition is 16 sectors (1k / sector) in size, 
    // need to erase the same prior to flashing the application software
    status = erase_flash(address, FLASH_APP_SECTORS);

    if (status != 0)
    {
        DEBUG("Failed to erase the flash block @ location %08x\r", address);
        return;
    }

    dbprintf("Erased the flash partition prior to updation\r");

    status = ROM_FlashProgram(data, address, datasize);

    if (status != 0)
    {
        DEBUG("Failed to program the flash block @ location %08x\r", address);
        return;
    }

    DEBUG("verify[0] is %08x\r", verify[0]);
    DEBUG("verify[1] is %08x\r", verify[1]);
    DEBUG("verify[2] is %08x\r", verify[2]);
    DEBUG("verify[3] is %08x\r", verify[3]);
    DEBUG("verify[4] is %08x\r", verify[4]);

    DEBUG("Successfully programmed the application software\r");
    return;
}

//
//*****************************************************************************
//
//! This function performs decryption of the application binary
//!
//! This function is called directly by the boot loader.
//!
//! \return Returns success status if decryption was successful, else error 
//
//*****************************************************************************

static void do_AES_decrypt(char *enc_data, int len,
	const br_block_cbcenc_class *ve,
	const br_block_cbcdec_class *vd)
{
    static unsigned char key[32];
    static unsigned char iv[16];
    size_t key_len;
    br_aes_gen_cbcdec_keys v_dc;
    const br_block_cbcdec_class **dc;

    if (ve->block_size != 16 || vd->block_size != 16
            || ve->log_block_size != 4 || vd->log_block_size != 4)
    {
        ERROR("AES failed: wrong block size\r");
        return;
    }

    dc = &v_dc.vtable;
    key_len = sizeof(aes_key);
    memcpy(key, aes_key, sizeof(aes_key));
#if 1
    vd->init(dc, key, key_len);
    memset(iv, 0, sizeof iv);
    vd->run(dc, iv, enc_data, len);
    DEBUG("enc_data[8] after decryption is %02x\r", enc_data[8]);
#endif
    DEBUG("done.\r");
}

//*****************************************************************************
//
//! This function performs SHA-256 hash computation of the given data
//!
//! This function is called directly by the boot loader.
//!
//! \return Returns nothing
//
//*****************************************************************************

static void compute_sha256(const void *data, int len, void *out)
{
	br_sha256_context csha256;

    br_sha256_init(&csha256);
	br_sha256_update(&csha256, data, len);
	br_sha256_out(&csha256, out);

    return;
}

//*****************************************************************************
//
//! This function performs verification of the SHA256 hash embedded in the
//! application image.
//!
//! This function is called directly by the boot loader.
//!
//! \return Returns success status if SHA256 check passed / error if not
//
//*****************************************************************************

int decrypt_swimage(unsigned char *image, unsigned int totalsize)
{
    unsigned int image_size = 0;
    static unsigned char hash[32];
    static unsigned char decrypted_hash[32];
    int i = 0;

    if (NULL == image)
    {
        ERROR("Application binary passed was NULL\r");
        return 1;
    }

    /* Get the actual size of the software image */
    memcpy(&image_size, &image[IMAGESIZE_OFFSET], IMAGESIZE);
    g_ui32ImageSize = image_size;

    DEBUG("Actual software image size (without header) is %d\r", g_ui32ImageSize);

    /* Decrypt the encrypted AES key & store it */
	if (!br_rsa_i15_private((unsigned char *)&image[AES_KEY_OFFSET], &RSA_SK)) {
		ERROR( "RSA decryption of AES key failed (1)\r");
        return 1;
	}
    memcpy(aes_key, &image[AES_KEY_OFFSET], sizeof(aes_key));

    for (i = 0; i<16; i++)
    {
        DEBUG("AES key[%d] is %02x\r", i, aes_key[i]);
    }

#if 0
    /* Decrypt the encrypted SHA256 hash */
	if (!br_rsa_i15_private((unsigned char *)&image[SHA256_OFFSET], &RSA_SK)) {
		ERROR( "RSA decryption of SHA256 hash failed (1)\r");
        return 1;
	}
#else
	if (!br_rsa_i15_pkcs1_vrfy((unsigned char *)&image[SHA256_OFFSET], SHA256_SIZE, BR_HASH_OID_SHA256, \
                sizeof (decrypted_hash), &RSA_PK, decrypted_hash))
	{
		ERROR("Signature verification failed (2)\r");
        return -1;
	}
#endif

    /* Decrypt the application with the AES key */ 
    do_AES_decrypt((char *)&image[DATA_OFFSET], image_size,
		&br_aes_big_cbcenc_vtable,
		&br_aes_big_cbcdec_vtable);

    // Perform SHA256 sum check on the app binary
    compute_sha256(&image[DATA_OFFSET], image_size, hash);

#if 0
    if (memcmp(decrypted_hash, hash, sizeof(hash)) != 0)
    {
        ERROR("Hash did not match with the transmitted hash!\r");
        return 1;
    }
#else
    for (i = 0; i<32; i++)
    {
        DEBUG("Recomputed Hash[%d] is %02x\r", i, hash[i]);
        //if (hash[i] != image[i+SHA256_OFFSET])
        if (hash[i] != decrypted_hash[i])
        {
        }
    }
#endif
    return 0;
}

//*****************************************************************************
//
//! This function performs the update on the selected port.
//!
//! This function is called directly by the boot loader or it is called as a
//! result of an update request from the application.
//!
//! \return Never returns.
//
//*****************************************************************************
// Uses the well-known XMODEM protocol for data transfer
int Updater(uint32_t start_address)
{
    int ret;

    // The downloaded app via XMODEM will be stored in RAM
    g_ui8AppBuffer = (unsigned char *)APP_START_ADDR;

    ret = 0;
    DEBUG("Entering xmodemReceive\r");
    g_ui32ImageSize = xmodemReceive(g_ui8AppBuffer, MAX_APP_SIZE);
    dbprintf("Size of data received was %d\r", g_ui32ImageSize);

    if (g_ui32ImageSize > MAX_APP_SIZE)
    {
        ERROR("App / image size exceeds maximum application size (%d) bytes\r", MAX_APP_SIZE);
        return -1;
    }

    ret = decrypt_swimage(g_ui8AppBuffer, g_ui32ImageSize);

    if (ret == 0)
    {
        dbprintf("Successfully decrypted & verified the integrity of the image\r");
        update_flash(start_address, (void *)(g_ui8AppBuffer+(DATA_OFFSET)), g_ui32ImageSize);
    }
    else
    {
        ERROR("Decryption / app integrity check failed!\r");
        return -1;
    }
    return 0;
}

//*****************************************************************************
